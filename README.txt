UPHP
====
by Rulo (Antonio) Kobashikawa Carrasco <akobashikawa@gmail.com>

uphp for Drupal.

uphp idea is act on html from outside, like unobstrusive javascript.
QueryPath allow this.

You can put static html in sites/default/files/html/

no HTML content is printed as is.
HTML content is processed to allow modules participation.

So, you can put entire static site and run it.
Then, use can use uphp to provide dynamic behavior.

UPHP HOOKS
----------

Modules can act on html content through uphp_hooks.

For example:

If url is http://mysite.example.com/html/echo.html&input=hello

then:
uphp template: html/echo.html
uphp action: html_echo
querypath: $vars['qp'] = htmlqp($template)

hooks to call:
hook_html_echo() // because action is html_echo
hook_html_ever() // ever

SUBMODULES
----------

UPHP INCLUDES
Provide include tag for html documents.

<include html="filename"/> To include filename as html
<include text="filename"/> To include filename as plain text
<include action="action_name"/> To include result of action_name as html
<include script="script_name"/> To include result of script_name as html